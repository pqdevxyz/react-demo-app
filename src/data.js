const data = [
    {
        id: 1,
        task: "Task number 1",
        date: "2019-09-07",
        completed: true,
        category: "red",
    },
    {
        id: 2,
        task: "Task number 2",
        date: "2019-09-02",
        completed: false,
        category: "teal",
    },
    {
        id: 3,
        task: "Task number 3",
        date: "2019-09-01",
        completed: false,
        category: "blue",
    },
    {
        id: 4,
        task: "Task number 4",
        date: "2019-08-28",
        completed: true,
        category: "teal",
    },
    {
        id: 5,
        task: "Task number 5",
        date: "2019-08-21",
        completed: true,
        category: "teal",
    },
    {
        id: 6,
        task: "Task number 6",
        date: "2019-08-21",
        completed: false,
        category: "red",
    },
    {
        id: 7,
        task: "Task number 7",
        date: "2019-08-21",
        completed: true,
        category: "blue",
    },
    {
        id: 8,
        task: "Task number 8",
        date: "2019-08-21",
        completed: false,
        category: "red",
    },

]

export default data