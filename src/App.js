import React from "react";
import TodoList from "./components/TodoList";

function App(){
    return (
        <div className="container mx-auto">
            <p className="font-thin text-2xl text-center my-4">React Todo List App</p>
            <TodoList/>
        </div>
    )
}

export default App