import React from "react";
import data from "../data";
import moment from "moment";
import TodoItem from "./TodoItem";

class TodoList extends React.Component{
    constructor(){
        super();
        this.state = {
            data: data,
            isLoading: true,
            showCompleted: false,
            showCategory: ""
        };
        this.handleTodoUpdate = this.handleTodoUpdate.bind(this);
        this.handleShowCompletedUpdate = this.handleShowCompletedUpdate.bind(this);
        this.handleShowCategoryUpdate = this.handleShowCategoryUpdate.bind(this);
        this.handleAddNew = this.handleAddNew.bind(this);
    }

    handleTodoUpdate(id){
        this.setState(current => {
            const updated = current.data.map(item => {
                if(item.id === id){
                    item.completed = !item.completed;
                }
                return item;
            })
            return {data: updated};
        }, () => {
            localStorage.setItem("todoData",JSON.stringify({"data":this.state.data}));
        })
    }
    handleShowCompletedUpdate(){
        this.setState({
            showCompleted: !this.state.showCompleted
        },() => {
            localStorage.setItem("showCompleted",JSON.stringify({"showCompleted":this.state.showCompleted}));
        })
    }
    handleShowCategoryUpdate(){
        this.setState({
            showCategory: document.getElementById("filter-category").value
        },() => {
            localStorage.setItem("showCategory",JSON.stringify({"showCategory":this.state.showCategory}));
        })
    }

    handleAddNew(){
        let lastId = 0;
        this.state.data.forEach(item => {
            if(item.id > lastId){
                lastId = item.id;
            }
        });
        let todoData = this.state.data;
        let newDate = document.getElementById("date").value !== "" ? moment(document.getElementById("date").value).format("YYYY-MM-DD"):moment().format("YYYY-MM-DD");
        todoData.push({
            id:lastId+1,
            task:document.getElementById("add").value,
            date: newDate,
            completed:false,
            category: document.getElementById("category").value,
        });
        todoData = todoData.sort((a,b) =>{
            if(a.date > b.date){
                return -1;
            }
            if(a.date < b.date){
                return 1;
            }else{
                return 0;
            }
        });
        this.setState({
            data:todoData
        }, () => {
            localStorage.setItem("todoData",JSON.stringify({"data":this.state.data}));
            document.getElementById("add").value = null;
        })
    }

    componentDidMount() {
        let lsShowCompleted = localStorage.getItem("showCompleted");
        let lsShowCategory = localStorage.getItem("showCategory");
        let lsTodoData = localStorage.getItem("todoData");

        if(lsShowCompleted !== null){
            lsShowCompleted = JSON.parse(lsShowCompleted).showCompleted;
            this.setState({
                showCompleted: lsShowCompleted,
            })
        }
        if(lsShowCategory !== null){
            lsShowCategory = JSON.parse(lsShowCategory).showCategory;
            this.setState({
                showCategory: lsShowCategory
            })
        }

        if(lsTodoData !== null){
            lsTodoData = JSON.parse(lsTodoData);
            lsTodoData.data = lsTodoData.data.sort((a,b) =>{
                if(a.date > b.date){
                    return -1;
                }
                if(a.date < b.date){
                    return 1;
                }else{
                    return 0;
                }
            });
            this.setState({
                data: lsTodoData.data,
                isLoading: false
            })
        }else{
            this.setState({
                isLoading: false
            })
        }
    }

    render() {
        if(!this.state.isLoading){
            const todoList = this.state.data.map(item => <TodoItem handleTodoUpdate={this.handleTodoUpdate} key={item.id} item={item} showCompleted={this.state.showCompleted} showCategory={this.state.showCategory}/>);
            return(
                <div className="mb-4">
                    <button className="bg-transparent border-teal-500 border-2 border-solid text-teal-500 hover:bg-teal-500 hover:text-white py-2 px-4 focus:outline-none mx-2 mb-5" onClick={this.handleShowCompletedUpdate}>{this.state.showCompleted ? "Hide":"Show"} Completed</button>
                    <div className="inline-block relative w-64">
                        <select value={this.state.showCategory} className="block appearance-none w-full pr-8 bg-transparent border-teal-500 border-2 border-solid text-teal-500 py-2 pl-4 focus:outline-none mx-2" id="filter-category" onChange={this.handleShowCategoryUpdate}>
                            <option value="">All Categories</option>
                            <option value="red">Red</option>
                            <option value="teal">Teal</option>
                            <option value="blue">Blue</option>
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </div>
                    </div>
                    {todoList}
                    <div className="flex">
                        <input className="appearance-none border w-full py-2 px-3 text-gray-700 focus:outline-none focus:bg-teal-100 mx-2 border-teal-500 placeholder-teal-700" id="add" type="text" placeholder="New task"/>
                        <input className="appearance-none border w-64 py-2 px-3 text-gray-700 focus:outline-none focus:bg-teal-100 mx-2 border-teal-500 placeholder-teal-700" id="date" type="date" placeholder="Date"/>
                        <select className="appearance-none bg-white border w-64 py-2 px-3 text-gray-700 focus:outline-none focus:bg-teal-100 mx-2 border-teal-500 placeholder-teal-700" id="category">
                            <option value="gray">No Category</option>
                            <option value="red">Red</option>
                            <option value="teal">Teal</option>
                            <option value="blue">Blue</option>
                        </select>
                        <button className="bg-teal-500 hover:bg-teal-700 text-white py-2 px-4 focus:outline-none mx-2" type="button" onClick={this.handleAddNew}>Add</button>
                    </div>
                </div>
            );
        }else{
            return(
                <div>
                    <div className="bg-teal-100 border-l-4 border-teal-500 text-teal-700 p-4 mx-2 mb-5" role="alert">
                        <p className="font-bold">Loading...</p>
                        <p>Please wait.</p>
                    </div>
                </div>
            );
        }
    }
}

export default TodoList;