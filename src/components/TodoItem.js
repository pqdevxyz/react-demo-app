import React from "react";
import moment from "moment";

function TodoItem(props) {
    const completedStyle = "italic line-through text-gray-500";
    let flexStyle = "hidden";
    if(props.showCategory === "" || props.showCategory === props.item.category){
        //Category blank (show all) or Item Category is same as Filter Category
        flexStyle = "block";
        if(!props.showCompleted && props.item.completed){
            //Hide Completed and Item is Completed
            flexStyle = "hidden";
        }
    }
    let categoryStyle = props.item.category !== null ? " border-" + props.item.category + "-300":" border-gray-300";
    return(
        <div className={"flex mb-4 " + flexStyle}>
            <div className={"w-full border-solid border-b-2 mx-2 flex items-center cursor-pointer h-12" + categoryStyle} onClick={(event) => props.handleTodoUpdate(props.item.id)}>
                <p className={props.item.completed ? completedStyle:null}>{props.item.task}</p>
            </div>
            <div className={"w-32 border-gray-300 border-solid border-b-2 mx-2 flex items-center justify-end cursor-pointer h-12" + categoryStyle} onClick={(event) => props.handleTodoUpdate(props.item.id)}>
                <p className={props.item.completed ? completedStyle:null}>{moment(props.item.date).format("DD MMM YY")}</p>
            </div>
        </div>
    );
}

export default TodoItem;